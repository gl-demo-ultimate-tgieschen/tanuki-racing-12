### Theme

Take a deeper dive into how GitLab enables you through shifting left and the various security reports that are all provided.

### Key Tasks to Complete

# Step 1: Merge Request Security Results

1. Right below the approvals section we can see that our  ***Code Quality, License Compliance, & Security scanning*** templates we included have generated full security reports unique to this branch for us to view. These reports are generated with each commit so we always know if we have introduced a new bug before its deployed out and disaster strikes.
  
2. Go ahead and take some time to expand each report and look through the results, then in the ***Security scanning*** section click on any of the critical/high vulnerabilities.
  
3. From this view we can see exactly where it occurred in the code & read up on why its a risk. After taking a look at this we can click the comment icon at the bottom to state that we checked it out and are not worried, then click **Add comment & dismiss**.
  
4. These vulnerabilities currently exist in our codebase as we have only added the scans to catch them. For now we will merge our scans into main to prevent more vulnerabilities from being committed in the future as we work to secure and prevent issues like this.

5. Next we will want to scroll down and merge our code. At the bottom of the request click **Merge** to kick off our pipeline. 
  
6. Once merged use the left hand navigation menu to click through **CICD > Pipelines** and click into the most recently kicked off pipeline. At this point your instructor will move onto the next section with a project that already has the code merged.
 
# Step 2: Merge Request Security Results
  
1. Now that your ***main*** pipeline has completed the reports under ***Secure*** have been generated. These reports will only be generated if you run a pipeline against main.
  
2. Use the left hand navigation menu to click through **Securite -> Security Dashboard**. This will bring up a dashboard view of all of the security vulnerabilities & their counts over time so you can track your work as you secure your project. This dashboard takes a long time to collect data so if yours still has no results your presenter will show you the dashboard of a deployed Tanuki Racing application [here](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/security/dashboard)
  
3. We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -> Vulnerability Report** to view the full report
  
4. Next look for the **Authorization bypass through user-controlled key** vulnerability by filtering the **_Tool_** to ***SAST-Semgrep***. Click into the vulnerability, then click the **Explain Vulnerability** button for an explanation on what an authorization bypass risk is and why our application is vulnerable using GitLab's Explain This Vulnerability funcationality.
  
5. At the end of the report check out the **_How to Fix the Vulnerability_** section and copy the proposed solution locally. We will use this knowledge later in the workshop.
  
6. If you are curious what triggered this response try clicking ***Show prompt*** to see the full prompt sent to GitLab duo to generate the suggested fix.
  
7. What if we wanted more context about the specific function above before we went and made a code change? Lets click the linked file in the **_Location_** section to be brought to our db.rb file.
  
8. Once in the db.rb file locate the line the authorization bypass vulnerability on line 42 and highlight the whole **_get_specific_** function.
  
9. You should then see a small question mark to the left of the code, click it.
  
10. On the right hand side there will now be a small pop up to explain what your highlighted code does in natural language. Try highlighting other code sections as well.

# Step 3: Review & Download SBOM report

1. Using the left hand navigation menu click through **Secure > Dependency list** to view all of the dependencies that are directly and indirectly included in your application. 

2. Click through a few of the pages and notice the components that are all directly/indirectly included in your application. 
  
3. Next to export these findings in the CycloneDX format we will use the left hand navigation menu to click through **Build > Pipelines** and find the pipeline most recently ran against **main** without clicking into it.

4. Next click the ***download*** icon next to that pipeline listing and then click the container_scanning:cyclonedx artifact. This will download your SBOM report in CycloneDX format. To learn more about CycloneDX format go [here](https://cyclonedx.org/)

> [See how you could have used GitLab to detect log4j](https://about.gitlab.com/blog/2021/12/15/use-gitlab-to-detect-vulnerabilities/)

# Step 4: License Compliance

1. We can also see the licenses detected in our project by using the left hand navigation menu to click back through the **Secure \> Dependency list**. To better see the included licenses select **Packager** in the top right filter (you may have to reverse the filter as well).
  
2. Lets say we decided we want to prevent the use of the LGPL-3.0 License. Using the left hand navigation menu click through the **Secure \> Policies** then click **New policy**.
  
3. Click **Select policy** under **Merge Request Approval Policy**
  
4. In the **New Merge Request Approval Policy form** that appears, at the top change from _Rule mode_ to _.yaml mode_ then replace the existing yaml with our new config below:

 ```
type: approval_policy
name: Deny LGPL-3.0 License
description: Denying unwanted license
enabled: true
rules:
  - type: license_finding
    match_on_inclusion_license: true
    license_types:
      - LGPL-3.0
    license_states:
      - newly_detected
      - detected
    branch_type: protected
actions:
  - type: require_approval
    approvals_required: 1
    user_approvers_ids:
      - 10994493
approval_settings:
  block_branch_modification: true
  prevent_pushing_and_force_pushing: false
```

5. Merge the new merge request into the existing security policy project.
  
6. _Remember to go back to your project using the breadcrumb, clicking on your group, then clicking on your project._
  
7. Now we will run a new pipeline for a MR, a new approval rule based on this license compliance policy will be added to prevent any software using the LGPL-3.0 license from being merged and the security bot will notify you that you have a policy violation.
